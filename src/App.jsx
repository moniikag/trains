import React, { Component } from 'react'
import moment from 'moment'
import './index.css'

const centralTrain = ['00', '20','40']
const circularTrain = ['00']
const northTrain = ['00', '12', '24', '36', '48']
const westTrain = ['00', '06', '12', '18', '24', '30', '36', '42', '48', '56']

export const timesEqual = (time1, time2) => {
  return moment(time1).format('hh:mm') === moment(time2).format('hh:mm')
}

export const isArrivingSoon = (trainTimes, time) => {
  return trainTimes.indexOf(moment(time).format('mm')) !== -1
}

class NextTrainIndicator extends Component {
  constructor(props) {
    super(props)
    const currentTime = new Date(Date.UTC(2017, 5, 2, 5, 0))
    this.state = {
      currentTime: currentTime,
      trains: [
        { destination: 'West Market', time: (moment(currentTime).add(6, 'minutes')) },
        { destination: 'West Market', time: (moment(currentTime).add(12, 'minutes')) },
        { destination: 'North Square', time: (moment(currentTime).add(12, 'minutes')) },
      ],
    }
  }

  componentWillMount() {
    this.updateClock = setInterval(() => {
      const newTime = moment(this.state.currentTime).add(1, 'minutes')
      this.setState({ currentTime: newTime })
      this.updateUpcomingTrains()
      this.removeOldTrains(newTime)}, 1000)
  }

  updateUpcomingTrains() {
    const nextTime = moment(this.state.currentTime).add(15, 'minutes')
    const trains = this.state.trains
    if(isArrivingSoon(centralTrain, nextTime)) {
      trains.push({ destination: 'Central Station', time: nextTime })
    }
    if(isArrivingSoon(circularTrain, nextTime)) {
      trains.push({ destination: 'Circular', time: nextTime })
    }
    if(isArrivingSoon(northTrain, nextTime) &&
      moment(nextTime).format('hh') >= '07' &&
      moment(nextTime).format('hh') <= '22') {
      trains.push({ destination: 'North Square', time: nextTime })
    }
    if(isArrivingSoon(westTrain, nextTime) &&
      ( moment(nextTime).format('hh') > '05' ||
        (moment(nextTime).format('hh') === '05' && moment(nextTime).format('mm') >= '30') ||
        moment(nextTime).format('hh') < '01' ||
        (moment(nextTime).format('hh') === '01' && moment(nextTime).format('mm') <= '30'))) {
      trains.push({ destination: 'West Market', time: nextTime })
    }
    this.setState({ trains })
  }

  removeOldTrains(newTime) {
    const { trains } = this.state
    while(trains[0] && timesEqual(trains[0].time, newTime)) { trains.splice(0,1) }
    this.setState({trains })
  }

  renderTrain(train, index, currentTime) {
    const minutes = moment(train.time - currentTime).format('mm')
    return (
      <tr key={`train${train.destination}-${train.time}`}>
        <td>{index+1}</td>
        <td>{train.destination}</td>
        <td>{minutes} {minutes === 1 ? 'min' : 'mins'}</td>
      </tr>
    )
  }

  render() {
    const { trains, currentTime } = this.state
    const displayedTrains = []
    for(let i = 0; i < 2; i++) {
      if(trains[i]) {
        displayedTrains.push(this.renderTrain(trains[i], i, currentTime))
      }
    }

    return (
      <div>
        <table id="trains-table">
          <thead>
            <tr>
              <th className="row-number"></th>
              <th className="destination">Destination</th>
              <th className="arrives-in">Arrives</th>
            </tr>
          </thead>
          <tbody>
            {displayedTrains}
          </tbody>
        </table>
        <div id="clock">
          <p>Virtual Time: {moment.utc(currentTime).format('hh:mm')}</p>
        </div>
      </div>
    )
  }
}

export default NextTrainIndicator
