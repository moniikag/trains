import React from 'react'
import ReactDOM from 'react-dom'
import NextTrainIndicator, * as fromNextTrainIndicator from './App'
import moment from 'moment'
import {shallow, mount} from 'enzyme'

const basicComponent = shallow(
  <NextTrainIndicator />
)

describe('NextTrainIndicator', () => {
  describe('component', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<NextTrainIndicator />, div);
    })

    it('is a div', () => {
      expect(basicComponent.is('div')).toBe(true)
    })

    it('sets initial state', () => {
      expect(basicComponent.instance().state.currentTime).not.toBe(null)
      expect(basicComponent.instance().state.trains.length).toEqual(3)
    })
  })

  describe('train schedule table', () => {
    it('renders two table rows inside tbody', () => {
      const tableBody = basicComponent.find('tbody')
      expect(tableBody.find('tr').length).toEqual(2)
    })

    it('renders index, destination and arrival time', () => {
      const tableBody = basicComponent.find('tbody')
      const firstTrainTr = tableBody.find('tr').at(0)
      expect(firstTrainTr.find('td').at(0).text()).toEqual('1')
      expect(firstTrainTr.find('td').at(1).text()).toEqual('West Market')
      expect(firstTrainTr.find('td').at(2).text()).toEqual('06 mins')
    })

    it('renders one train when only one train present', () =>{
      basicComponent.setState({ trains: [
        { destination: 'Central Station', time: new Date() }
      ]})
      const tableBody = basicComponent.find('tbody')
      expect(tableBody.find('tr').length).toEqual(1)
    })

    it('renders empty table when no trains available', () =>{
      basicComponent.setState({ trains: []})
      const tableBody = basicComponent.find('tbody')
      expect(tableBody.find('tr').length).toEqual(0)
    })
  })

  describe('virtual time clock', () => {
    it('renders clock div, with starting time: 05:00', () => {
      expect(basicComponent.find('#clock').text()).toBe('Virtual Time: 05:00')
    })

    it('updates displayed time when component state updates', () => {
      const time = basicComponent.instance().state.currentTime
      const newTime = moment(time).add('minutes', 15)
      basicComponent.setState({ currentTime: newTime })
      expect(basicComponent.find('#clock').text()).toBe('Virtual Time: 05:15')
    })
  })

  describe('isArrivingSoon', () => {
    const testTrainTimes = ['35', '55']

    it('returns true if train is arriving at indicated time', () => {
      const time = new Date(2017, 5, 2, 11, 35)
      expect(fromNextTrainIndicator.isArrivingSoon(testTrainTimes, time))
        .toBe(true)
    })

    it('returns false if train is not arriving at indicated time', () => {
      const time = new Date(2017, 5, 2, 11, 40)
      expect(fromNextTrainIndicator.isArrivingSoon(testTrainTimes, time))
        .toBe(false)
    })
  })
})

